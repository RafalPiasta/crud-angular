import {Component, Inject, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators, NG_ASYNC_VALIDATORS} from "@angular/forms";
import {ApiService} from "../services/api.service";
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog'

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {

  productCondition =
    [
      "Nowy",
      "Używany",
      "Odnowiony"
    ];
  productForm!: FormGroup;
  actionButton: string = "Zapisz";

  constructor(
    private formBuilder: FormBuilder,
    private api: ApiService,
    @Inject(MAT_DIALOG_DATA) public editData: any,
    private dialogRef: MatDialogRef<DialogComponent>
  ) {

  }

  ngOnInit(): void {
    this.productForm = this.formBuilder.group({
      productName: ['', Validators.required],
      category: ['', Validators.required],
      date: ['', Validators.required],
      condition: ['', Validators.required],
      price: ['', Validators.required],
      comment: ['', Validators.required]
    });

    if (this.editData) {
      this.actionButton = "Aktualizuj";
      this.productForm.controls['productName'].setValue(this.editData.productName);
      this.productForm.controls['category'].setValue(this.editData.category);
      this.productForm.controls['date'].setValue(this.editData.date);
      this.productForm.controls['condition'].setValue(this.editData.condition);
      this.productForm.controls['price'].setValue(this.editData.price);
      this.productForm.controls['comment'].setValue(this.editData.comment);
    }
  }

  addProduct() {
    if (!this.editData) {
      if (this.productForm.valid) {
        this.api.postProduct(this.productForm.value)
          .subscribe({
            next: (res) => {
              alert("Dodano produkt do listy");
              this.productForm.reset();
              this.dialogRef.close('save');
            },
            error: () => {
              alert("Błąd podczas dodawania produktu")
            }
          })
      }
    } else {
      this.updateProduct()
    }
  }

  updateProduct() {
    this.api.putProduct(this.productForm.value, this.editData.id)
      .subscribe({
        next: (res) => {
          alert("Produkt został poprawnie edytowany");
          this.productForm.reset();
          this.dialogRef.close('update');
        },
        error: () => {
          alert("Błąd podczas edytowania produktu");
        }
      })
  }
}
